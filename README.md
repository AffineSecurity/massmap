# massmap - a simple and efficient scanning script

### Requirements
Your system must have nmap and masscan installed in a location listed in $PATH. The script will check to ensure this is the case.  
**Please note**: it is highly recommended you build masscan from source (https://github.com/robertdavidgraham/masscan) as there are known issues with versions of masscan installed from package managers. Please refer to the masscan github page for instructions on building from source.

### Script
Usage: ./massmap.sh [IP ADDRESS] [INTERFACE]

Massmap will write out one file - masscan.txt - to be used as input for the nmap port option. For this reason, please ensure you execute masscan in a directory with write privileges.

