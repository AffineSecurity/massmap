#!/bin/bash


### Functions 

usage()
{
  echo "massmap - a simple and efficient scanning too"
  echo "Created by AffineSecurity"
  echo -e "\nusage: ./massmap.sh [IP ADDRESS] [INTERFACE NAME]"
}

bin_check()                                               
{                                                      
  mis_bin=""  
  req_bins=(masscan nmap)
  for item in ${req_bins[*]}; do
    result=$(which $item)
    if [[ -z $result ]]; then 
        echo "WARNING: missing binary $item from path"                                                              
        exit 0                                            
      fi          
  done   
}

ip_check()
{
  if [[ $ip_addr =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
    OIFS=$IFS
    IFS='.'
    ip=($ip_addr)
    IFS=$OIFS
    [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
      && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
      true 
  else
    echo "IP address invalid - please use a valid IP address format"
    exit 0
  fi
}

int_check()
{
  IFS='
  '
  status=1
  int_avail=$(ip -o l show | cut -d ":" -f 2)
  for item in $int_avail;do
    if [[ $interface = $item ]]; then
      status=0
      return $status
    fi
  done
  if [[ $status -ne 0 ]]; then
    echo "Interface name invalid, please enter one of the following interfaces: "
    for item in "$int_avail"; do
      echo "$item"
    done
    exit 0
  fi
}

masscan_run()
{
  echo -e "\nBeginning scan of host $ip_addr"
  massmap_output="$(locate massmap.sh | cut -d "." -f 1).txt"
  sudo masscan --rate=1000 -p 1-65535 $ip_addr -e $interface -oL $massmap_output
}

checkports()
{
  ports=$(grep open $massmap_output | wc -l)
  if [ $ports -eq 0 ];then
    echo "No open ports were discovered with masscan. Exiting..."
    exit 0
  fi
}

nmap_run()
{
  echo -e "\nMasscan discovered $ports open ports. Beginning nmap scan..."
  sudo nmap -Pn -A -p $(cut -d " " -f 3 -s $massmap_output | awk -v RS='' '{gsub("\n", ","); print}') $ip_addr
}


### Main
if [[ $2 = "" ]];then
  usage
  exit 0
fi

ip_addr=$1
interface=$2
ports=""
massmap_output=""

bin_check
ip_check
int_check
masscan_run
checkports
nmap_run

